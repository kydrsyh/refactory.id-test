const express = require("express");
const config = require("./src/config/");
var fs = require("fs");
var path = require("path");
var morgan = require("morgan");

const app = express();
const containers = []

const accessLogStream = fs.createWriteStream(path.join(__dirname, "server.log"), {
    flags: "a"
});

app.use(express.json())

app.use(morgan("[:date[clf]] success :status :method :url " , {
    stream: accessLogStream
}));

app.post("/create", (req, res) => {
    const container = {
        counter: req.body.counter,
        "X-RANDOM" : req.header('x-random')
    }
    containers.push(container)

    res.status(201).send(containers)
});

app.get('/', (req, res) => {
    res.status(200).send(containers)
});

app.listen(config.port);
console.log(`App listening on http://localhost:${config.port}`);