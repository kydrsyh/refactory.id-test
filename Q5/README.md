# QUESTION NUMBER 5

# HOW TO RUN THIS CODE

- First u need to npm install to install all dependencies from this file
- you can use npm start to run this program
- or node app.js

- after that, open another terminal, then run node client.js
- it will automatically generate counter to data storage.
- the logging will be saved to server.log, in this case i use morgan to do the logging.
- to check the data, u can access it from browser and type : http:localhost:3000/

# ERROR

- The logging format still not the same with the expected output.
- Still find a method to display the data to the log
