const fetch = require("node-fetch");
var InsertAPIURL = 'http://localhost:3000/create';

function randomString(length) {
  var result           = '';
  var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  var charactersLength = characters.length;
  for ( var i = 0; i < length; i++ ) {
     result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

let counter = 1
function callAPI(){
    
    var headers={
        'x-random': randomString(8),
        'Content-Type':'application/json'
      };

      var Data = {
        counter : counter++
      }
      fetch(InsertAPIURL,{
        method:'POST',
        headers,
        body:JSON.stringify(Data)
      })
      .then((res)=> {
          console.log(res.status);
      })
      .catch((err)=>{
        console.log('Error', err)
      })
}

setInterval(callAPI, 60*1000);
      