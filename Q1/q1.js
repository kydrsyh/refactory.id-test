const readline = require('readline')
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
})

function inputResto() {
  rl.question(`Resto name: `, restoName => {
    rl.question(`Tanggal: `, date => {
      rl.question(`Cashier name: `, cashierName => {
        inputItems(restoName, cashierName, date)
      })
    })

  })
}

var item = []
var price = []
var result = 0
function inputItems(restoName, cashierName, date) {
  rl.question(`item: `, items => {
    rl.question(`price: `, prices => {

      item.push({item: items, price: prices})
      result += Number(prices)

      rl.question(`Add next item ? (y/n)\n`, answer => {
        if(answer.toLowerCase() == "y") {

          inputItems(restoName, cashierName, date)

        } else printPayment(restoName, cashierName, date)
      })
    })
  })
}

function printPayment(restoName, cashierName, date, items, prices) {
  if(restoName.length < 30) {
    space = (30 - restoName.length)/2
    console.log(`\n${restoName.padStart(restoName.length + space, ' ')}\n`);
  }
  console.log("Tanggal    :", date);
  console.log("Nama Kasir :", cashierName);
  console.log("==============================");
  
  item.forEach(e => {
    let currency = "Rp."
    space = (30 - (Number(e.item.length) + Number(e.price.length) + 3));
    console.log(e.item + currency.padStart(currency.length + space, '.') + e.price)})
  
  if(result.toString().length + 8 < 30) {
    let currency = "Rp."
    space = (30 - (Number(result.toString().length) + 8));
    console.log("\nTotal" + currency.padStart(currency.length + space, '.') + result);
  }
 rl.close()
}

inputResto()