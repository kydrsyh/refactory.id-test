# QUESTION NUMBER 1

I solve this question using node.js

- Open the folder
- then run the app by node q1.js

# INPUT

- For input in terminal, i use readline module
- input resto name (String)
- input date (String ex:15/02/2021 15:30)
- input cashier name (String)
- input item (String)
- input price (Integer)
- get the print receipt
