# QUESTION NUMBER 4

# HOW TO RUN THIS CODE

- First u need to npm install to install all dependencies from this file
- you can use npm start to run this program
- or node q4.js
- after that open the browser and open http://localhost:3000
- and get the login data from your github account

# DEBUGGING

- The first step of debugging i fix the .env name folder
- Input the API client_id, client seceret etc from Github API
- I fix config format and exports the parameter to be able to get the env variables
- Some typo on module.exports in several file
- in userInfoServices.js i fix the .then catch with async await to get the return data in authCallbackServices.js
- Fix typo in module.exports userInfoServices.js
- Fix some typo in authCallbackServices.js "resp"
