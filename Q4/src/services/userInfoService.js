const axios = require("axios");
const config = require("../config");

async function getUserInfo(token) {
    let user = await axios({
        method: "get",
        url: `${config.apiUrl}/user`,
        headers: {
          Authorization: "token " + token,
        },
      })
    return user.data
}

module.exports = {
  getUserInfo: getUserInfo
}
