# QUESTION NUMBER 2

I solve this question using node.js

- Open the folder
- then run the app by node q2.js

# INPUT

- Input data from question, JSON file

# ANSWER

- Mostly i solve the JSON manipulation using filter method from javascript
- and nested loop to filter array inside of array
- For date format i slice the year and make the if statement to comparae it
- For searching i use includes instead of regex
