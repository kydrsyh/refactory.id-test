let data = [{
    "id": 323,
    "username": "rinood30",
    "profile": {
      "full_name": "Shabrina Fauzan",
      "birthday": "1988-10-30",
      "phones": [
        "08133473821",
        "082539163912"
      ]
    },
    "articles:": [{
        "id": 3,
        "title": "Tips Berbagi Makanan",
        "published_at": "2019-01-03T16:00:00"
      },
      {
        "id": 7,
        "title": "Cara Membakar Ikan",
        "published_at": "2019-01-07T14:00:00"
      }
    ]
  },
  {
    "id": 201,
    "username": "norisa",
    "profile": {
      "full_name": "Noor Annisa",
      "birthday": "1986-08-14",
      "phones": []
    },
    "articles:": [{
        "id": 82,
        "title": "Cara Membuat Kue Kering",
        "published_at": "2019-10-08T11:00:00"
      },
      {
        "id": 91,
        "title": "Cara Membuat Brownies",
        "published_at": "2019-11-11T13:00:00"
      },
      {
        "id": 31,
        "title": "Cara Membuat Brownies",
        "published_at": "2019-11-11T13:00:00"
      }
    ]
  },
  {
    "id": 42,
    "username": "karina",
    "profile": {
      "full_name": "Karina Triandini",
      "birthday": "1986-04-14",
      "phones": [
        "06133929341"
      ]
    },
    "articles:": []
  },
  {
    "id": 201,
    "username": "icha",
    "profile": {
      "full_name": "Annisa Rachmawaty",
      "birthday": "1987-12-30",
      "phones": []
    },
    "articles:": [{
        "id": 39,
        "title": "Tips Berbelanja Bulan Tua",
        "published_at": "2019-04-06T07:00:00"
      },
      {
        "id": 43,
        "title": "Cara Memilih Permainan di Steam",
        "published_at": "2019-06-11T05:00:00"
      },
      {
        "id": 58,
        "title": "Cara Membuat Brownies",
        "published_at": "2019-09-12T04:00:00"
      }
    ]
  }
]

// Number 1 criteria
console.log("1. Find users who don't have any phone numbers:");
let filterByPhone = data.filter((el) => {
  return el.profile.phones.length == 0
}).forEach(e => console.log(e.profile.full_name))

// Number 2 criteria
console.log("\n2. Find users who have articles:");
let filterByArticles = data.filter((el) => {
  return el["articles:"].length > 0
}).forEach(e => console.log(e.profile.full_name))

// Number 3 criteria
console.log(`\n3. Find users who have "annis" on their name:`)
let filterByText = data.filter((el) => {
  return el.profile.full_name.toLocaleLowerCase().includes("annis")
}).forEach(e => console.log(e.profile.full_name))

// Number 4 criteria
console.log(`\n4. Find users who have articles on the year 2020:`)
let filterByHaveArticle = []
for (let i = 0; i < data.length; i++) {
  for (let k = 0; k < data[i]["articles:"].length; k++) {
    let year = parseInt(data[i]["articles:"][k].published_at.slice(0, 4))

    if (year == 2020) {
      filterByHaveArticle.push(data[i]["articles:"][k].title)
    }
  }
}
filterByHaveArticle.forEach(e => console.log(e))

// Number 5 criteria
console.log(`\n5. Find users who are born in 1986:`)
let filterByBorn = data.filter((el) => {
  return el.profile.birthday.toLocaleLowerCase().includes("1986")
}).forEach(e => console.log(e.profile.full_name))

// Number 6 criteria
console.log(`\n6. Find articles that contain "tips" on the title:`)
let filterByArticlesTitle = []
for (let i = 0; i < data.length; i++) {
  for (let k = 0; k < data[i]["articles:"].length; k++) {
    if (data[i]["articles:"][k].title.includes("Tips")) {
      filterByArticlesTitle.push(data[i]["articles:"][k].title)
    }
  }
}
filterByArticlesTitle.forEach(e => console.log(e))

// Number 7 criteria
console.log(`\n7. Find articles published before August 2019:`)
let filterByPublishedDate = []
for (let i = 0; i < data.length; i++) {
  for (let k = 0; k < data[i]["articles:"].length; k++) {
    let year = parseInt(data[i]["articles:"][k].published_at.slice(0, 4))
    let month = parseInt(data[i]["articles:"][k].published_at.slice(5, 7))

    if (year < 2019) {
      filterByPublishedDate.push(data[i]["articles:"][k].title)
    }

    if (year == 2019 && month < 8) {
      filterByPublishedDate.push(data[i]["articles:"][k].title)
    }

  }
}
filterByPublishedDate.forEach(e => console.log(e))