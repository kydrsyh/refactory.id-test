let data = [{
        "inventory_id": 9382,
        "name": "Brown Chair",
        "type": "furniture",
        "tags": [
            "chair",
            "furniture",
            "brown"
        ],
        "purchased_at": 1579190471,
        "placement": {
            "room_id": 3,
            "name": "Meeting Room"
        }
    },
    {
        "inventory_id": 9380,
        "name": "Big Desk",
        "type": "furniture",
        "tags": [
            "desk",
            "furniture",
            "brown"
        ],
        "purchased_at": 1579190642,
        "placement": {
            "room_id": 3,
            "name": "Meeting Room"
        }
    },
    {
        "inventory_id": 2932,
        "name": "LG Monitor 50 inch",
        "type": "electronic",
        "tags": [
            "monitor"
        ],
        "purchased_at": 1579017842,
        "placement": {
            "room_id": 3,
            "name": "Lavender"
        }
    },
    {
        "inventory_id": 232,
        "name": "Sharp Pendingin Ruangan 2PK",
        "type": "electronic",
        "tags": [
            "ac"
        ],
        "purchased_at": 1578931442,
        "placement": {
            "room_id": 5,
            "name": "Dhanapala"
        }
    },
    {
        "inventory_id": 9382,
        "name": "Alat Makan",
        "type": "tableware",
        "tags": [
            "spoon",
            "fork",
            "tableware"
        ],
        "purchased_at": 1578672242,
        "placement": {
            "room_id": 10,
            "name": "Rajawali"
        }
    }
]

console.log("1. Find items in the Meeting Room")
let findItemByRoom = data.filter((el) => {
    return el.placement.name.includes("Meeting Room")
}).forEach(e => console.log(e.name))

console.log("\n2. Find all electronic devices")
let findItemByElectronic = data.filter((el) => {
    return el.type.includes("electronic")
}).forEach(e => console.log(e.name))

console.log("\n3. Find all the furniture")
let findItemByFurniture = data.filter((el) => {
    return el.type.includes("furniture")
}).forEach(e => console.log(e.name))

console.log("\n4. Find all items were purchased on 16 Januari 2020")
let findItemByPurchased = []
for (let i = 0; i < data.length; i++) {
    if (new Date(data[i].purchased_at * 1000).toDateString() == "Thu Jan 16 2020") {
        findItemByPurchased.push(data[i])
    }
}
findItemByPurchased.forEach(e => console.log(e.name))

console.log("\n5. Find all items with brown color")
let findItemByTags = data.filter((el) => {
    return el.tags.includes("furniture")
}).forEach(e => console.log(e.name))