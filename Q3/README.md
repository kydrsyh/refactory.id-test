# QUESTION NUMBER 3

I solve this question using node.js

- Open the folder
- then run the app by node q3.js

# INPUT

- Input data from question, JSON file

# ANSWER

- Mostly i solve the JSON manipulation using filter method from javascript
- For date format is epoch so i need to multiply it to 1000 seconds
- For searching i use includes instead of regex
